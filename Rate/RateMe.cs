﻿using UnityEngine;
using System.Collections;

public class RateMe : MonoBehaviour {

	public UISprite BTSprite;
	public UISprite[] StarSprites;

	public GameObject RateButton;


	// Use this for initialization
	void Start () {
		BTSprite.spriteName = "";
		RateButton.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private int Starval;

	 void OnStar(GameObject button)
	{


	}

	void OnStarHover(GameObject button)
	{
		RateButton.SetActive(true);

		Starval = int.Parse(button.name);
		
		if(Starval <= 3)
		{
			BTSprite.spriteName = "contact";
			
		}else{
			BTSprite.spriteName = "rate";
			
		}


		for(int i = 0; i < Starval; i++)
		{
			StarSprites[i].spriteName = "star";
		}
		
		for(int j = Starval; j < StarSprites.Length;j++)
		{

			StarSprites[j].spriteName = "star-base";

		}
		

	}


	public void OnRate()
	{
		if(Starval <= 3)
		{
#if UNITY_IPHONE
//			EtceteraBinding.showMailComposer( "dressup@risinghigh.net", "Girl Dressup Feedback", "Please share your valuable feedback and suggestions so that we can improve the game.", true );
#else

//			EtceteraAndroid.showEmailComposer( "dressup@risinghigh.net", "Girl Dressup Feedback", "Please share your valuable feedback and suggestions so that we can improve the game.", true );
#endif

		}else{
#if UNITY_IPHONE
			Application.OpenURL("https://itunes.apple.com/us/app/dress-up-queen/id947415360?ls=1&mt=8");
#else
			Application.OpenURL("https://play.google.com/store/apps/details?id=com.wanaka.dozerfrenzyg");
#endif
		}

	}

	public void OnDismiss()
	{
		this.gameObject.SetActive(false);
	}

}
