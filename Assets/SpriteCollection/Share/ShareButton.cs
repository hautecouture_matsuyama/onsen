﻿using UnityEngine;
using System.Collections;

public class ShareButton : MonoBehaviour {

    public UIManager share;

    public void OnClick()
    {
        if (gameObject.name == "Facebook")
        {
            share.ShareFacebook();
        }

        if (gameObject.name == "Line")
        {
            share.ShareLINE();
        }

        if (gameObject.name == "Twitter")
        {
            share.ShareTwitter();
        }
    }
}
