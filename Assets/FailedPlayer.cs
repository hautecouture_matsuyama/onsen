﻿using UnityEngine;
using System.Collections;

public class FailedPlayer : MonoBehaviour {

    [SerializeField]
    private Sprite failedSprite;

    private SpriteRenderer mySprite;

    private float stopTime;

    //前のポジション
    private Vector3 prePos;

    Rigidbody2D rigid2d;

    void Start()
    {
        mySprite = GetComponent<SpriteRenderer>();
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "LowerBoundry")
        {
            mySprite.sprite = failedSprite;
        }
    }

    void Update()
    {
        if(this.gameObject.GetComponent<Rigidbody2D>() != null) rigid2d = GetComponent<Rigidbody2D>();

        if (this.gameObject.transform.parent.gameObject.name != "Crwas(Clone)")
        {
            if (rigid2d.velocity.magnitude == 0)
            {
                stopTime += Time.deltaTime;
            }

            else
            {
                stopTime = 0;
            }

            if(stopTime >= 2.5f)
            {
                Camera.main.GetComponent<MainScript>().StartCoroutine(Camera.main.GetComponent<MainScript>().StartGameEndAnimation(0.1f));
            }
            

        }
    }

 


}
