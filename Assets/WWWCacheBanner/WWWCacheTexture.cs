using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using LitJson;

public class WWWCacheTexture : MonoBehaviour {

    private List<CacheData> dataList;

    public class CacheData
    {
        public string name;
        public string textureUrl;
        public string transitionUrl;
        public bool awake;
        public int count;
    }

    private const string AwakeCount = "AwakeCount";
    private const string CurrentIndex = "CurrentIndex";

    //General
    private string textureName;

    private enum TexCall { AwakeCount, Time }
    private TexCall texCall;

    private string textureLink;
    public string transitionLink
    {
        get;
        private set;
    }
    private string texturePath;
    private int count;

    [SerializeField]
    private string jsonLink = "http://100apps.s3.amazonaws.com/000_Original/000_BannerData.txt";

    [SerializeField]
    private int originalPixelWidth=980, originalPixelHeight=195;

    IEnumerator sCoroutine;
    IEnumerator lCoroutine;

    void Awake()
    {
        //コルーチンの登録.
        sCoroutine = StartLoad();
        lCoroutine = LoadTexture();
    }


    void Start()
    {
        transitionLink = "http://apps.hautecouture.jp";
    }

    //オブジェクト有効化に合わせてコルーチン開始.
    void OnEnable()
    {
        if (dataList == null)
        {
            StartCoroutine(sCoroutine);
        }
        else
        {
            StartCoroutine(lCoroutine);
        }
    }

    //オブジェクト無効化に合わせてコルーチン停止.
    void OnDisable()
    {
        if (dataList == null)
        {
            StopCoroutine(sCoroutine);
        }
        else {
            StopCoroutine(lCoroutine);
        }
    }

    /// <summary>
    /// ネット上からバナーをダウンロードして表示する.
    /// ネットにつながらない時はデフォルトのテクスチャのみで対応.
    /// </summary>
	IEnumerator StartLoad () {
        dataList = new List<CacheData>();
        CacheData[] jsonData = null;
        
        using (WWW www = new WWW(jsonLink))
        {
            yield return www;
            if (www.error == null)
            {
                jsonData = JsonMapper.ToObject<CacheData[]>(www.text);
            }
        }
        
        if (jsonData == null) yield break;
        dataList.AddRange(jsonData);
        StartCoroutine(LoadTexture());
	}

    IEnumerator LoadTexture()
    {
        //初期化.
        int index = PlayerPrefs.GetInt(CurrentIndex);
        if (index >= dataList.Count) index = 0;

        count = dataList[index].count;
        texCall = dataList[index].awake ? TexCall.AwakeCount : TexCall.Time;

        //シーン初期化回数でバナーを切り替える場合.
        int awakeCount = 0;
        if (texCall == TexCall.AwakeCount)
        {
            awakeCount = PlayerPrefs.GetInt(AwakeCount) + 1;
            if (awakeCount >= count)
            {
                if (++index >= dataList.Count) index = 0;
                PlayerPrefs.SetInt(CurrentIndex, index);
                PlayerPrefs.SetInt(AwakeCount, 0);
            }
            else
            {
                PlayerPrefs.SetInt(AwakeCount, awakeCount);
            }
        }
        //時間指定の場合.
        else
        {
            yield return new WaitForSeconds(count);
            if (++index >= dataList.Count) index = 0;
            PlayerPrefs.SetInt(CurrentIndex, index);
        }

        texCall = dataList[index].awake ? TexCall.AwakeCount : TexCall.Time;
        textureName = dataList[index].name;
        textureLink = dataList[index].textureUrl;

        //以下で適用.
        texturePath = Application.persistentDataPath + "/" + textureName;
        Texture2D texture = new Texture2D(originalPixelWidth, originalPixelHeight);
        

        //ネットワーク接続時はテクスチャダウンロード.
        do
        {
            if (File.Exists(texturePath)) break;
            if (Application.internetReachability == NetworkReachability.NotReachable) break;
            using (WWW www = new WWW(textureLink))
            {
                yield return www;
                if (www.error == null)
                {
                    File.WriteAllBytes(texturePath, www.bytes);
                }
            }
        } while (false);

        //気休めの軽量化.
        yield return new WaitForSeconds(0.2f);

        //非接続、エラー発生時は処理を抜ける.
        if (!File.Exists(texturePath))
        {
            yield break;
        }
        else
        {
            byte[] imageBytes = File.ReadAllBytes(texturePath);
            Texture2D tex2D = new Texture2D(originalPixelWidth, originalPixelHeight);

            if (tex2D.LoadImage(imageBytes))
            {
                texture = tex2D;
            }
            //読み取りエラーが起こっているので処理を抜ける.
            else
            {
                yield break;
            }
        }

        //テクスチャの適用.
        ApplyTexture(texture);

        //遷移先の変更.
        transitionLink = dataList[index].transitionUrl;

        //要素が一つより多く、時間指定の場合は再帰的に呼び出す.
        if (dataList.Count >= 2 && texCall == TexCall.Time)
            StartCoroutine(LoadTexture());
    }



    void ApplyTexture(Texture2D texture)
    {
        //uGUI版.
        var image = GetComponent<Image>();
        if (image)
        {
            image.sprite = Sprite.Create(texture, new Rect(0, 0, originalPixelWidth, originalPixelHeight), Vector2.zero);
            return;
        }

        //Sprite版.
        var sprite = GetComponent<SpriteRenderer>();
        if (sprite)
        {
            sprite.sprite = Sprite.Create(texture, new Rect(0, 0, originalPixelWidth, originalPixelHeight), new Vector2(0.5f,0.5f));
            return;
        }

        //Renderer版.
        var renderer = GetComponent<Renderer>();
        if (renderer)
        {
            GetComponent<Renderer>().material.mainTexture = texture;
            return;
        }
    }

    public void OpenTransitionURL()
    {
        Application.OpenURL(transitionLink);
    }

    void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }
}
