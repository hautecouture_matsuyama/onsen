﻿using UnityEngine;
using System.Collections;

public class MagnetScript : MonoBehaviour
{
    public float velocityMultiplier = 400;

    private Rigidbody2D torso;
    private float distance;
	private bool roboReleased = false;

    [SerializeField]
    private bool velocity;

    
    // Use this for initialization
    void Start()
    {
        GetComponent<BoxCollider2D>().enabled = false;

        torso = gameObject.GetComponent<Rigidbody2D>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (roboReleased && other.transform.CompareTag("RobotObject"))
        {
           initializeTorso(other.transform.parent.gameObject);
            if (torso)
            {
                distance = transform.position.x - torso.transform.position.x;
            }
            else
            {
                initializeTorso(other.transform.parent.gameObject);
            }
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (roboReleased && other.transform.CompareTag("RobotObject"))
        {
            

            if (torso)
            {
                
                distance = transform.position.x - torso.transform.position.x;
                float force = Mathf.Sign(distance) * velocityMultiplier;
                /* force を状況に応じて変更する案を実装してたけど、現実的じゃないので各磁石の強さを変更する方向にする.
                 * エフェクトがいる.
                Debug.Log("torsoSign:" + Mathf.Sign(torso.velocity.x) + " forceSign:" + Mathf.Sign(force));
                if (Mathf.Sign(torso.velocity.x) != Mathf.Sign(force))
                {
                    force *= 3.5f;
                }*/

                if (velocity)
                {

                    other.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(force, 0));

                }

                else
                {
                    other.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(force*-1, 0));
                }
               torso.AddForce(new Vector2(100,0));

                

            }
            else
                initializeTorso(other.transform.parent.gameObject);
        }
    }
    
    void OnTriggerExit2D(Collider2D other)
    {
        torso = null;
    }

    private void initializeTorso(GameObject collidingObject)
    {
        foreach (Transform child in collidingObject.transform)
        {
            if (child.name.Equals("touhu"))
            {
                torso = child.gameObject.GetComponent<Rigidbody2D>();
            }

        }
    }
    
    public void isRoboreleased(bool value)
    {
       GetComponent<BoxCollider2D>().enabled = true;
        roboReleased = value;
    }
}
