﻿using UnityEngine;
using System.Collections;
//script will be ob lower boundry and crusher.... to detect if robot is no longer able to crush
public class GameEndDetectionScript : MonoBehaviour
{

    [SerializeField]
    private float endTime;

    public static int cnt = 0;

    void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (transform.CompareTag("LowerBoundry") && collision.transform.CompareTag("RobotObject") && cnt == 0)
        {
            Camera.main.GetComponent<MainScript>().StartCoroutine(Camera.main.GetComponent<MainScript>().StartGameEndAnimation(endTime));
            Debug.Log("CRUSHHHHHHHHHHH");
            cnt++;
        }
        else if (transform.CompareTag("Crusher")) {

            Camera.main.GetComponent<MainScript>().setCrusherCollisionTime();
        }

    }


    void Show()
    {
        
            NendUnityPlugin.AD.NendAdInterstitial.Instance.Show();
        
    }
}
