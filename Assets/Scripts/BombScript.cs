﻿using UnityEngine;
using System.Collections;
//Bomb is hurdle, if collides with robo ,it will be destroyed make sure radius and power used for explosion force is private
public class BombScript : MonoBehaviour
{
		private float radius = 5.0F;
		private float power = 5000.0F;
		private GameObject robot;

        [SerializeField]
        private Sprite ChangeSprite;

        private SpriteRenderer thisSprite;

    void Start()
        {
            thisSprite = gameObject.GetComponent<SpriteRenderer>();
        }


		void OnCollisionEnter2D (Collision2D collision)
		{
			if (collision.transform.CompareTag ("RobotObject")) {

                thisSprite.sprite = ChangeSprite;
                
                //正常
                Camera.main.GetComponent<MainScript>().StartCoroutine(Camera.main.GetComponent<MainScript>().StartGameEndAnimation(2));
                
                //Camera.main.GetComponent<MainScript> ().BombCollided (transform.position);
                Destroy (collision.gameObject);
                    //robot = collision.transform.parent.gameObject;
                    //Camera.main.GetComponent<SoundManager> ().playBombSound ();
                    ////joints will be destroyed and explosion force will be added
                    //foreach (Transform child in robot.transform)
                    //        child.tag = "Untagged";
                    //foreach (HingeJoint hJoint in robot.GetComponentsInChildren<HingeJoint>())
                    //        Destroy (hJoint as HingeJoint);
    
                    //Vector3 explosionPos = transform.position;
                    //Collider[] colliders = Physics.OverlapSphere (explosionPos, radius);
                    //foreach (Collider hit in colliders) {
                    //        if (hit && hit.GetComponent<Rigidbody>())
                    //                hit.GetComponent<Rigidbody>().AddExplosionForce (power, explosionPos, radius, 3.0F);
                    //}
                    //Camera.main.GetComponent<MainScript> ().BombCollided (transform.position);
                    //Destroy (gameObject);
			}
		}
}
    