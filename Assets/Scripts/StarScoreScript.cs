﻿using UnityEngine;
using System.Collections;

public class StarScoreScript : MonoBehaviour {
	
	// Use this for initialization
	
	public GameObject starSoundGameEnd;
	int noOfStarsDeserved = 0;
	SoundManager manageSound;
	
	
	void OnEnable()
	{
		manageSound = GameObject.FindObjectOfType(typeof(SoundManager)) as SoundManager;
		
		if (manageSound.isSoundOn == true) 
			StartCoroutine (wait());
		
	}
	
	
	IEnumerator wait()
	{
		
		for (int count=0; count<=PlayerPrefs.GetInt ("boltTaken")-1; count++) {
			Instantiate (starSoundGameEnd);
			yield return new WaitForSeconds (0.2f);
		}
	}
}


