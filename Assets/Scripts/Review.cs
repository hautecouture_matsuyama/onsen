﻿using UnityEngine;
using System.Collections;

public class Review : MonoBehaviour {

	public Sprite StarClose;
	public Sprite StarOpen;
	public GameObject[] Stars;
	public GameObject Rate;
	private string LastName;
	public GameObject[] ObjectsToDisable;
	public string BundleId;
	private string email="raj@miraclestudiosgames.com";
	public string subject="bla";
	public string body="body";
	public Sprite Contact;
	public Sprite RateB;
	// Use this for initialization

	void OnMouseDown () 
	{
//		foreach (GameObject obj in Stars)
//		{
//			obj.SetActive(true);
//		}
//		Rate.SetActive (true);
		this.transform.GetChild (0).transform.gameObject.SetActive (true);
		this.GetComponent<Animator> ().enabled = false;
		foreach (GameObject obj1 in ObjectsToDisable)
		{
			obj1.SetActive(false);
		}
		if(Stars[4].GetComponent<SpriteRenderer>().sprite==StarOpen)
			Rate.gameObject.GetComponent<SpriteRenderer>().sprite=RateB;
		else
			Rate.gameObject.GetComponent<SpriteRenderer>().sprite=Contact;
	}

	void TouchedOnStar(string name)
	{
		if(name!=LastName || name=="Cross")
		{
			Debug.Log (": "+name);
			if(name!="rate")
			LastName=name;

			foreach (GameObject obj in Stars)
			{
				obj.GetComponent<SpriteRenderer>().sprite=StarClose;
			}
			Stars[0].GetComponent<SpriteRenderer>().sprite=StarOpen;
			if(LastName=="Star5" || LastName == "Star4")
			{
				Rate.gameObject.GetComponent<SpriteRenderer>().sprite=RateB;
			}
			else
			{
				Rate.gameObject.GetComponent<SpriteRenderer>().sprite=Contact;
			}
			switch (name)
			{
			case "Star1":
			Stars[0].GetComponent<SpriteRenderer>().sprite=StarOpen;
			break;
			case "Star2":
				Stars[1].GetComponent<SpriteRenderer>().sprite=StarOpen;;
			break;
			case "Star3":
			Stars[1].GetComponent<SpriteRenderer>().sprite=StarOpen;
				Stars[2].GetComponent<SpriteRenderer>().sprite=StarOpen;
				break;
			case "Star4":
			Stars[1].GetComponent<SpriteRenderer>().sprite=StarOpen;
			Stars[2].GetComponent<SpriteRenderer>().sprite=StarOpen;
				Stars[3].GetComponent<SpriteRenderer>().sprite=StarOpen;
				break;
			case "Star5":
			Stars[1].GetComponent<SpriteRenderer>().sprite=StarOpen;
			Stars[2].GetComponent<SpriteRenderer>().sprite=StarOpen;
				Stars[3].GetComponent<SpriteRenderer>().sprite=StarOpen;
				Stars[4].GetComponent<SpriteRenderer>().sprite=StarOpen;
				break;
			case"rate":
				RateFunc();
				break;
			case "Cross":
				Cross();
				break;
			}
		}
	}
	void RateFunc()
	{
		if (LastName == "Star5" || LastName == "Star4") 
		{
			Application.OpenURL("market://details?id="+BundleId);
		}
		else
		{Debug.Log("Rate");
			Application.OpenURL("mailto:" + email + "?subject:" + subject + "&body:" + body);
		}
//		foreach (GameObject obj in Stars)
//		{
//			obj.SetActive(false);
//		}
//		Rate.SetActive (false);
		this.transform.GetChild (0).transform.gameObject.SetActive (false);
		this.GetComponent<Animator> ().enabled = true;
		foreach (GameObject obj1 in ObjectsToDisable)
		{
			obj1.SetActive(true);
		}
	}
	void Cross()
	{
		this.transform.GetChild (0).transform.gameObject.SetActive (false);
		this.GetComponent<Animator> ().enabled = true;
		foreach (GameObject obj1 in ObjectsToDisable)
		{
			obj1.SetActive(true);
		}
	}

}
