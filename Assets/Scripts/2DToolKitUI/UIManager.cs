﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
//manages navigation in UI
public class UIManager : UIBaseController
{
    #region PUBLIC_VARS
		public GameObject mainMenuPanel, levelSelectionPanel, levelSelectionPanel2, levelCompletePanel, levelFailPanel, Bg, levesObject;
		public GameObject[] Levels, Stars;
		public SoundManager soundManagerScriptRef;
        public Sprite[] TipsSprite;
    #endregion

    #region PRIVATE_VARS
		private GameObject currentPanel, levelsButtonsParent, currentLevelRef, levelToDestroy;
		public int currentLevel = 0, maxLevels, unlockedLevels = 1;
        
    #endregion

    #region UNITY_CALLBACKS

        private List<Transform> currentLevelPanel = new List<Transform>();


	void Start()
	{

        foreach (Transform child in levesObject.transform)
        {
            currentLevelPanel.Add(child);
        }



	}
		void Awake ()
		{
				Bg.SetActive (true);
				maxLevels = Levels.Length;
				currentPanel = mainMenuPanel;
                ShowWindow (currentPanel.transform);
				unlockedLevels = getUnlockedLevels ();

				soundManagerScriptRef.playMainMenuMusic ();
		}

		void OnEnable ()
		{
				UIDelegate.onLevelButtonClick += OnLevelButtonClick;
				UIDelegate.onLevelComplete += OnLevelComplete;
				UIDelegate.onLevelFail += OnLevelFail;

		}

		void OnDisable ()
		{
				UIDelegate.onLevelButtonClick -= OnLevelButtonClick;
				UIDelegate.onLevelComplete -= OnLevelComplete;
				UIDelegate.onLevelFail -= OnLevelFail;
		}

    #endregion

    [SerializeField]
       private Targetscript targetScripts;

    [SerializeField]
    private GameObject howtoPanel;

    string shareurl ="";

#if UNITY_ANDROID
    string text = "https://play.google.com/store/apps/details?id=com.hautecouture.onsen&hl=ja";
#elif UNITY_IPHONE
    strign text = "https://itunes.apple.com/us/app/wen-quanmanjuukun-tangmegurikarenda/id1137017642?l=ja&ls=1&mt=8";
#endif


    #region PUBLIC_FUNCTIONS


		public void OnLevelComplete ()
		{
                
            
				Bg.SetActive (true);

				destroyCurrentLevel ();
				Camera.main.transform.position = new Vector3 (0, 0, -50);
				soundManagerScriptRef.playLevelCompleteMusic ();

				if (currentLevel == getUnlockedLevels ())
						increaseUnlockedLevels ();

				HideWindow (currentPanel.transform);
				currentPanel = levelCompletePanel;
				ShowWindow (currentPanel.transform);

                if (currentLevel == maxLevels)
                    levelCompletePanel.transform.Find("NextBtn").gameObject.SetActive(false);
                else
                    levelCompletePanel.transform.Find("NextBtn").gameObject.SetActive(true);

				displayLevelCompleteStars ();
		}

		public void displayLevelCompleteStars ()
		{
				int collectedStars = Camera.main.GetComponent<MainScript> ().getBoltTaken ();

				if (collectedStars < 4 && collectedStars > (PlayerPrefs.GetInt (("Level" + currentLevel), 0)))
						PlayerPrefs.SetInt (("Level" + currentLevel), collectedStars);

        //レベルの花マル
                for (int i = 0; i < Stars.Length; i++)
                {
                    if (i + 1 <= collectedStars) 
                        Stars[i].GetComponent<Renderer>().enabled = false;
                }

            

                switch (collectedStars)
                {
                    case 0:
                        Stars[0].GetComponent<Renderer>().enabled = true;
                        break;
                    case 1:
                        Stars[1].GetComponent<Renderer>().enabled = true;
                        break;
                    case 2:
                        Stars[2].GetComponent<Renderer>().enabled = true;
                        break;
                    case 3:
                        Stars[3].GetComponent<Renderer>().enabled = true;
                        break;
                }
		}

		public void OnLevelFail ()
		{

            if (currentLevel % 2 == 0)
                Debug.Log("GAMEOVER");
				Bg.SetActive (true);
				destroyCurrentLevel ();
				Camera.main.transform.position = new Vector3 (0, 0, -50);
				soundManagerScriptRef.playLevelFailMusic ();

				HideWindow (currentPanel.transform);
				currentPanel = levelFailPanel;
				ShowWindow (currentPanel.transform);
		}

		public void loadNextLevel ()
		{

		if(currentLevel%2 == 0)


		Camera.main.depth = 2;
				HideWindow (currentPanel.transform);
				currentLevel++;
				loadLevel (currentLevel);

		}

		public void replayLevel ()
		{
            GameEndDetectionScript.cnt = 0;

				HideWindow (currentPanel.transform);
				Camera.main.GetComponent<MainScript> ().StopAllCoroutines ();
				destroyCurrentLevel ();
                loadLevel(currentLevel);

                targetScripts.ChangeTargetSprite();

		}
	
	#endregion

    #region PRIVATE_FUNCTIONS
 

        //レベルの読み込み
		private void loadLevel (int levelIndex)
		{
            if (levelIndex == 1)
            { 
                howtoPanel.SetActive(true);
                
            }

            targetScripts.ChangeTargetSprite();
				Bg.SetActive (false);
				soundManagerScriptRef.playGameMusic ();
				currentLevelRef = (GameObject)Instantiate (Levels [levelIndex - 1], new Vector3 (0, 0, 0), Quaternion.identity);
				currentLevelRef.transform.tag = "Scene";
				Camera.main.GetComponent<MainScript> ().loadInitialData (0.0f, levelIndex, currentLevelRef);
		}

		private void destroyCurrentLevel ()
		{
				if (!currentLevelRef)
						return;
				levelToDestroy = currentLevelRef;
				levelToDestroy.SetActive (false);
				Destroy (levelToDestroy);

		}

		private int getUnlockedLevels ()
		{
				unlockedLevels = PlayerPrefs.GetInt ("Unlocked Levels_GameName", 1);
				return unlockedLevels;
		}

		private void increaseUnlockedLevels ()
		{
				if (getUnlockedLevels () < maxLevels) {
						unlockedLevels = PlayerPrefs.GetInt ("Unlocked Levels_GameName", 1) + 1;
						PlayerPrefs.SetInt ("Unlocked Levels_GameName", unlockedLevels);
						PlayerPrefs.Save ();
				}
		}

    #endregion

    #region GUI_EVENT_LISTENER

        public void OnPlay()
        {
            if (currentPanel != null)
                HideWindow(currentPanel.transform);
            currentPanel = levelSelectionPanel;
            ShowWindow(currentPanel.transform);
        }

		public void OnBack ()
		{
				Camera.main.transform.position = new Vector3 (0, 0, -50);
				HideWindow (currentPanel.transform);
				currentPanel = mainMenuPanel;
				ShowWindow (currentPanel.transform);
				soundManagerScriptRef.playMainMenuMusic ();
		}

    //ステージパネルアクティブにする

    //アクティブになるたびに呼ばれる

    IEnumerator Test()
        {
            yield return new WaitForSeconds(0.05f);
            for (int i = 0; i < 20; i++)
            {
                currentLevelPanel[i].Find("Level_Waku").gameObject.SetActive(false);
            }

            currentLevelPanel[getUnlockedLevels()-1].Find("Level_Waku").gameObject.SetActive(true);
        }


		public void showLevelSelectionScene ()
		{


            //for (int i = 0; i < 20; i++)
            //{
            //    currentLevelPanel[i].FindChild("Level_Waku").gameObject.SetActive(false);
            //}
            StartCoroutine(Test());
                Camera.main.GetComponent<MainScript>().StopAllCoroutines();

			                           	if(GameObject.Find("UIParent/LevelCompletePanel") != null)
				GameObject.FindObjectOfType<ReviewPanelScript> ().reactiveReviewDialogue ();		//reactive dialoguebox

				HideWindow (currentPanel.transform);
				currentPanel = levelSelectionPanel;
				ShowWindow (currentPanel.transform);

				Bg.SetActive (true);
				destroyCurrentLevel ();
				Camera.main.transform.position = new Vector3 (0, 0, -50);
				soundManagerScriptRef.playLevelSelectionMusic ();

				levelsButtonsParent = levelSelectionPanel.transform.GetChild (0).gameObject;
				int unlockedLevels_ = getUnlockedLevels ();

               //次プレイする位置を表示
                //currentLevelPanel[unlockedLevels_ - 1].FindChild("Level_Waku").gameObject.SetActive(true);

                for (int index = 0; index < levelsButtonsParent.transform.childCount; index++)
                {
                    levelsButtonsParent.transform.GetChild(index).gameObject.GetComponent<UICoreButton>().ToggleLock(unlockedLevels_);
                    Debug.Log("アンロック解除");
                }
		}


   


        public void OnBackToSelectionScene1()
        {
            HideWindow(currentPanel.transform);
            currentPanel = levelSelectionPanel;
            ShowWindow(currentPanel.transform);
        }

        public void showLevelSelectionScene2()
        {
            HideWindow(currentPanel.transform);
            currentPanel = levelSelectionPanel2;
            ShowWindow(currentPanel.transform);
        }

		void OnLevelButtonClick (int levelIndex)
		{
				currentPanel = levelSelectionPanel;
				HideWindow (currentPanel.transform);
                currentPanel = levelSelectionPanel2;
                HideWindow(currentPanel.transform);

				Camera.main.depth = 2;

				currentLevel = levelIndex;
				loadLevel (levelIndex);
		}

    #endregion

        public Texture2D shareTexture;

        public void ShareTwitter()
        {
            string url = text;
            //UM_ShareUtility.TwitterShare("温泉まんじゅうくん　ゆめぐりカレンダー " + currentLevel + "ステージクリア！\n" + text, shareTexture);
        }

        public void ShareLINE()
        {
            string msg = "温泉まんじゅうくん　ゆめぐりカレンダー" + currentLevel + " ステージクリア\n" + text;
            string url = "http://line.me/R/msg/text/?" + WWW.EscapeURL(msg);
            Application.OpenURL(url);
        }

        public void ShareFacebook()
        {
            
        }

        string FeedLink = "https://play.google.com/store/apps/details?id=com.hautecouture.onsen&hl=ja";
        string FeedLinkName = "";
        string FeedLinkDescription = "";
        string FeedPicture = "https://100apps.s3.amazonaws.com/000_Original/ShareImage/onsen_header.jpg";
        private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();

}