﻿using System.Collections;
using UnityEngine;

/// <summary>
/// UI Delegate
/// </summary>
/// <Author>
/// Maulik Kaloliya
/// </Author>
public class UIDelegate
{
    #region PUBLIC_VARS
    /// <summary>
    /// Delegate for listen level button click
    /// </summary>
    /// <param name="levelIndex"></param>
    
	public delegate void OnLevelComplete();
	public delegate void OnLevelFail();
	
	public static event OnLevelComplete onLevelComplete;
	public static event OnLevelFail onLevelFail;
	
	public delegate void OnLevelButtonClick(int levelIndex);
	
    public static event OnLevelButtonClick onLevelButtonClick;
	
    #endregion

    #region PUBLIC_FUNCTIONS
    /// <summary>
    /// Raise OnLevelButtonClick event
    /// </summary>
    /// <param name="levelIndex">Level index</param>
    public void RaiseOnLevelButtonClickEvent(int levelIndex){
        if (onLevelButtonClick != null)
            onLevelButtonClick(levelIndex);
    }
	
	
	public void RaiseOnLevelComplete()
	{
		//Debug.Log("OnLevelComplete");
		if(onLevelComplete != null)
		{
			//Debug.Log("inside if");
			onLevelComplete();		
		}
	}
	
	public void RaiseOnLevelFail()
	{
		//Debug.Log("OnLevelFail");
		if(onLevelFail != null)
		{
			//Debug.Log("inside if of fail");
			onLevelFail();		
		}
	}
	
	
	
    #endregion

}
