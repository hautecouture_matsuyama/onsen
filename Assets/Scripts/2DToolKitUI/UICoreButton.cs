﻿using UnityEngine;
using System.Collections;
/// <summary>
/// UI Core Button that handles level selection functionality
/// </summary>
/// <Author>
/// Maulik Kaloliya
/// </Author>
public class UICoreButton : UIBaseController
{
    #region PUBLIC_VARS
    /// <summary>
    /// Level index
    /// </summary>
    public int loadLevelIndex;
    /// <summary>
    /// Delegate for listen level button click
    /// </summary>
    public static event System.Action<int> OnLevelButtonClick;
    #endregion

    #region PRIVATE_VARS
    /// <summary>
    /// Get reference of UIItem
    /// </summary>
    private tk2dUIItem button;
    /// <summary>
    /// UI delegate object
    /// </summary>
    private UIDelegate uiDelegate;
    private GameObject lockSprite;
    private GameObject[] Stars= new GameObject[3];


	
    #endregion

    #region UNITY_CALLBACKS

    // Awake is called when the script instance is being loaded
    void Awake()
    {
        //Create new ui delegate reference
        uiDelegate = new UIDelegate();
        //Get UIItem reference
        button = GetComponent<tk2dUIItem>();
		if (transform.childCount > 2)
		{
			lockSprite = transform.GetChild(2).gameObject;
            for (int index = 6; index <= 8; index++)
            {
                Stars[index-6] = transform.GetChild(index).gameObject;
            }
		}
		
		if (lockSprite)
		{
            if (loadLevelIndex <= PlayerPrefs.GetInt("Unlocked Levels_GameName", 1)){
				lockSprite.SetActive(false);
              //  showCurrentLevelStar();
            }
			else
			{
				lockSprite.SetActive(true);
			}
		}
    }

    // This function is called when the object becomes enabled and active
    void OnEnable()
    {
        //Register onclick listener
        button.OnClick += OnButtonClick;
    }

    // This function is called when the behaviour becomes disabled or inactive
    void OnDisable()
    {
        //Deregister onclick listener
        button.OnClick -= OnButtonClick;
    }

    #endregion

    #region OTHER_EVENT_LISTENER
    void OnButtonClick()
    {
        uiDelegate.RaiseOnLevelButtonClickEvent(loadLevelIndex);
		
    }
    #endregion

   
	public void ToggleLock(int unlockedLevels)
	{
  
        if (loadLevelIndex <= unlockedLevels)
        {
			lockSprite.SetActive (false);
            showCurrentLevelStar();
		}

	}


    //星を付ける
    private void showCurrentLevelStar()
    {
        int collectedStars = (PlayerPrefs.GetInt(("Level" + loadLevelIndex), 0));


        for (int i = 0; i < 3; i++)
        {
            Stars[i].GetComponent<Renderer>().enabled = false;
        }

        switch(collectedStars)
        {
            case 1:
                Stars[0].GetComponent<Renderer>().enabled = true;
                break;
            case 2:
                Stars[1].GetComponent<Renderer>().enabled = true;
                break;
            case 3:
                Stars[2].GetComponent<Renderer>().enabled = true;
                break;
        }

    }
}
