﻿using UnityEngine;
using System.Collections;
//script will be on crusher to detect level completex
public class Targetscript : MonoBehaviour
{
		private bool crushed = false;

        [SerializeField]
        private Sprite clearSprite;

        [SerializeField]
        private Sprite defaultSprite;

        private SpriteRenderer mySpriteRenderer;


    void Start()
        {
            mySpriteRenderer = GetComponent<SpriteRenderer>();
        }

		void OnCollisionEnter2D(Collision2D collision)
		{
				if (collision.transform.CompareTag ("RobotObject")) {
						Camera.main.GetComponent<SoundManager> ().playCrusherSound ();
						collision.transform.parent.gameObject.SetActive (false);
						Destroy (collision.transform.parent.gameObject);
						Camera.main.GetComponent<MainScript> ().instantiateCrushedText ();
                        mySpriteRenderer.sprite = clearSprite;
                        Debug.Log("GOAL");
				}
		}

    public void ChangeTargetSprite()
        {
            mySpriteRenderer.sprite = defaultSprite;

        }
}
