﻿using UnityEngine;
using System.Collections;
//handles sound
public class SoundManager : MonoBehaviour
{
    public AudioClip mainMenuMusic, gamePlayMusic, levelSelectMusic, levelCompleteMusic, levelFailMusic, starSound;
    public AudioClip anvilSound, boltSound, bombSound, crusherSound;

    public bool isSoundOn = true;

	public void playStarMusic()
	{
		playMusic (starSound);
	}

    public void playLevelSelectionMusic()
    {
        playMusic(levelSelectMusic);
    }
    public void playMainMenuMusic()
    {
        playMusic(mainMenuMusic);
    }

    public void playGameMusic()
    {
        playMusic(gamePlayMusic);
    }

    public void playLevelFailMusic()
    {
        if (isSoundOn)
            gameObject.GetComponent<AudioSource>().PlayOneShot(levelFailMusic);
    }

    public void playLevelCompleteMusic()
    {
        if (isSoundOn)
            gameObject.GetComponent<AudioSource>().PlayOneShot(levelCompleteMusic);
    }
    void stopBackGroundMusic()
    {
        if (isSoundOn)
            gameObject.GetComponent<AudioSource>().Stop();
    }

    private void playMusic(AudioClip musicClip)
    {
        if (isSoundOn)
        {
            if (gameObject.GetComponent<AudioSource>().clip == musicClip && gameObject.GetComponent<AudioSource>().isPlaying)
                return;

            stopBackGroundMusic();
            gameObject.GetComponent<AudioSource>().clip = musicClip;
            gameObject.GetComponent<AudioSource>().loop = true;
            gameObject.GetComponent<AudioSource>().Play();
        }
    }
    public void playAnvilSound()
    {
        if (isSoundOn)
            gameObject.GetComponent<AudioSource>().PlayOneShot(anvilSound);
    }


    public void playCrusherSound()
    {
        if (isSoundOn)
            gameObject.GetComponent<AudioSource>().PlayOneShot(crusherSound);
    }
    public void playBoltSound()
    {
        if (isSoundOn)
            gameObject.GetComponent<AudioSource>().PlayOneShot(boltSound);
    }
    public void playBombSound()
    {
        if (isSoundOn)
            gameObject.GetComponent<AudioSource>().PlayOneShot(bombSound);
    }
    void setSoundPlay(bool value)
    {
        isSoundOn = value;
        if (!isSoundOn)
            gameObject.GetComponent<AudioSource>().Stop();
        else
            playMainMenuMusic();
    }


}
