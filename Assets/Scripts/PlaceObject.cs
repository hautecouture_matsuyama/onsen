using UnityEngine;
using System.Collections;

//script for placing object according to different screen,for example objects like back button that shouls be placed at top left in every screen size.
public class PlaceObject : MonoBehaviour
{
		private Vector3 position;
		private float screenWidth, screenHeight;
		private float colliderXSize = 0, colliderYSize = 0;
		public OBJECT_POSITION objectPosition = OBJECT_POSITION.BOTTOM_LEFT;
			
		public enum OBJECT_POSITION
		{
				BOTTOM_LEFT = 1,
				BOTTOM_RIGHT = 2,
				TOP_LEFT = 3,
				TOP_RIGHT = 4,
				RIGHT_SIDE=5,
				LEFT_SIDE=6,
				BELOW_SIDE=7,
				ABOVE_SIDE=8

		}
		public GameObject targetObject;
		private Vector3 targetPosition;

		void Start ()
		{
				screenWidth = Screen.width;
				screenHeight = Screen.height;
				if (GetComponent<BoxCollider2D> () != null) {
						colliderXSize = GetComponent<BoxCollider2D> ().size.x;
						colliderYSize = GetComponent<BoxCollider2D> ().size.y;
						GetComponent<BoxCollider2D> ().isTrigger = true;
				} else if (GetComponent<BoxCollider> () != null) {
						colliderXSize = GetComponent<BoxCollider> ().size.x;
						colliderYSize = GetComponent<BoxCollider> ().size.y;
						GetComponent<BoxCollider> ().isTrigger = true;
				} else {
						Debug.Log ("no collider attached in " + transform.name);
						return;
				}

				position = Camera.main.WorldToScreenPoint (transform.position);

				if (objectPosition == OBJECT_POSITION.BOTTOM_LEFT)
						placeAtBottomLeft ();
				else if (objectPosition == OBJECT_POSITION.BOTTOM_RIGHT)
						placeAtBottomRight ();
				else if (objectPosition == OBJECT_POSITION.TOP_LEFT)
						placeAtTopLeft ();
				else if (objectPosition == OBJECT_POSITION.TOP_RIGHT)
						placeAtTopRight ();
				else if (objectPosition == OBJECT_POSITION.RIGHT_SIDE)
						placeAtRightSide ();
				else if (objectPosition == OBJECT_POSITION.ABOVE_SIDE)
						placeAtAboveSide ();
				else if (objectPosition == OBJECT_POSITION.LEFT_SIDE)
						placeAtLeftSide ();
				else
						Debug.Log ("specify position for " + transform.name);



//				if (bottomLeft)
//						placeAtBottomLeft ();
//				else if (bottomRight)
//						placeAtBottomRight ();
//				else if (topLeft)
//						placeAtTopLeft ();
//				else if (topRight)
//						placeAtTopRight ();
		}
	
		private void placeAtTopLeft ()
		{
				transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (0, screenHeight, position.z));
				transform.position = new Vector3 (transform.position.x + colliderXSize / 2, transform.position.y - colliderYSize / 2, transform.position.z);
		}
	
		private void placeAtTopRight ()
		{
				transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (screenWidth, screenHeight, position.z));
				transform.position = new Vector3 (transform.position.x - colliderXSize / 2, transform.position.y - colliderYSize / 2, transform.position.z);
		}

		private void placeAtBottomLeft ()
		{
				transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (0, 0, position.z));
				transform.position = new Vector3 (transform.position.x + colliderXSize / 2, transform.position.y + colliderYSize / 2, transform.position.z);
		}

		private void placeAtBottomRight ()
		{
				transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (screenWidth, 0, position.z));
				transform.position = new Vector3 (transform.position.x - colliderXSize / 2, transform.position.y + colliderYSize / 2, transform.position.z);
		}

		private void placeAtRightSide ()
		{
				
				if (targetObject != null) {
						Invoke ("placeAtRightSide_Late", 1);
				} else
						Debug.Log ("specify target object for " + transform.name);
		}

		private void placeAtRightSide_Late ()
		{
				float colliderXSizeTargetObject = 0, colliderYSizeTargetObject = 0;
		
				if (targetObject.GetComponent<BoxCollider2D> () != null) {
						colliderXSizeTargetObject = targetObject.GetComponent<BoxCollider2D> ().size.x;
						colliderYSizeTargetObject = targetObject.GetComponent<BoxCollider2D> ().size.y;
				} else if (targetObject.GetComponent<BoxCollider> () != null) {
						colliderXSizeTargetObject = targetObject.GetComponent<BoxCollider> ().size.x;
						colliderYSizeTargetObject = targetObject.GetComponent<BoxCollider> ().size.y;
				} else {
						Debug.Log ("target object of " + transform.name + "Dont have collider");
				}
				targetPosition = new Vector3 ((targetObject.transform.position.x + (colliderXSizeTargetObject / 2) + (colliderXSize / 2)), targetObject.transform.position.y, 0);
				transform.position = targetPosition;
		}

		private void placeAtLeftSide ()
		{
		
				if (targetObject != null) {
						Invoke ("placeAtLeftSide_Late", 1);
				} else
						Debug.Log ("specify target object for " + transform.name);
		}
	
		private void placeAtLeftSide_Late ()
		{
				float colliderXSizeTargetObject = 0, colliderYSizeTargetObject = 0;
		
				if (targetObject.GetComponent<BoxCollider2D> () != null) {
						colliderXSizeTargetObject = targetObject.GetComponent<BoxCollider2D> ().size.x;
						colliderYSizeTargetObject = targetObject.GetComponent<BoxCollider2D> ().size.y;
				} else if (targetObject.GetComponent<BoxCollider> () != null) {
						colliderXSizeTargetObject = targetObject.GetComponent<BoxCollider> ().size.x;
						colliderYSizeTargetObject = targetObject.GetComponent<BoxCollider> ().size.y;
				} else {
						Debug.Log ("target object of " + transform.name + "Dont have collider");
				}
				targetPosition.z = transform.position.z;
				targetPosition = new Vector3 ((targetObject.transform.position.x - (colliderXSizeTargetObject / 2) - (colliderXSize / 2)), targetObject.transform.position.y, targetPosition.z);
				transform.position = targetPosition;
		}

		private void placeAtAboveSide ()
		{
		
				if (targetObject != null) {
						Invoke ("placeAtAboveSide_Late", 1);
				} else
						Debug.Log ("specify target object for " + transform.name);
		}
		
		private void placeAtAboveSide_Late ()
		{
				float colliderXSizeTargetObject = 0, colliderYSizeTargetObject = 0;
		
				if (targetObject.GetComponent<BoxCollider2D> () != null) {
						colliderXSizeTargetObject = targetObject.GetComponent<BoxCollider2D> ().size.x;
						colliderYSizeTargetObject = targetObject.GetComponent<BoxCollider2D> ().size.y;
				} else if (targetObject.GetComponent<BoxCollider> () != null) {
						colliderXSizeTargetObject = targetObject.GetComponent<BoxCollider> ().size.x;
						colliderYSizeTargetObject = targetObject.GetComponent<BoxCollider> ().size.y;
				} else {
						Debug.Log ("target object of " + transform.name + "Dont have collider");
				}
				targetPosition = new Vector3 (targetObject.transform.position.x, (targetObject.transform.position.y + (colliderYSizeTargetObject / 2) + (colliderYSize / 2)), 0);
				transform.position = targetPosition;
		}
		
}