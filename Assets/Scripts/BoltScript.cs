﻿using UnityEngine;
using System.Collections;
//bolt object script manages behavious of bolt
public class BoltScript : MonoBehaviour
{
		private bool taken = false;
		void Start ()
		{
				transform.position += new Vector3 (0, 0, -1f);
				//GetComponent<BoxCollider2D> ().size = new Vector2 (1, 1);
		}

		void OnTriggerEnter2D (Collider2D other)
		{
				if (!taken && other.transform.CompareTag ("RobotObject")) {
						taken = true;
						Camera.main.GetComponent<SoundManager> ().playBoltSound ();
						Camera.main.GetComponent<MainScript> ().increaseBoltTaken ();
						Camera.main.GetComponent<MainScript> ().instantiateCollisionParticles (transform.position);
						Destroy (gameObject);
				}
		}
}
