﻿using UnityEngine;
using System.Collections;

public class ReviewPanelScript : MonoBehaviour {
	
	GameObject BackButton, RetryButton, NextLevelButton, stars;	//to be deactivated
	GameObject reviewStar1, reviewStar2, reviewStar3, reviewStar4, reviewStar5, SendReviewButton, dialogueBG;		//to be activated
	GameObject toggleReview;

	void mainReviewButtonToggle () {

		//toggleReview = GameObject.Find ("UIParent/LevelCompletePanel/googleReviewMainButton");
		
	}

    static int cnt = 0;

    void OnEnable()
    {

    }

	void Awake()
	{
		SendReviewButton = GameObject.Find ("SendReview");		//the whole dialogue box containing all rating panel elements
		if(SendReviewButton != null)
			SendReviewButton.SetActive (false);
	
	}

	public void DeactivatateReviewButtons()
	{
	//	Debug.Log ("Deactivated");
		if(GameObject.Find ("UIParent/LevelCompletePanel/Stars/LevelCompleteStarFilled2/CFX4 Secret Place Loop 1") != null)
		stars = GameObject.Find ("UIParent/LevelCompletePanel/Stars/LevelCompleteStarFilled2/CFX4 Secret Place Loop 1");


		//stars.SetActive (false);
	}

	public void DeactivatateReviewButtonsFromDialogueBox()
	{
	//	Debug.Log ("DeactivatedFromDialogue");
		SendReviewButton.SetActive (false);

		BackButton.SetActive (true);

		if(RetryButton != null)
		RetryButton.SetActive (true);

		if(NextLevelButton != null)
		NextLevelButton.SetActive (true);

		if(stars != null)
		stars.SetActive (true);
		//this.gameObject.SetActive (true);

		toggleReview = GameObject.Find ("googleReviewMainButton");
		toggleReview.GetComponent<tk2dUIItem>().SendMessageOnClickMethodName = "ActiveReviewPanel";
		
}


	public void ActiveReviewPanel () {
	//	Debug.Log ("avtivated");
		SendReviewButton.SetActive (true);

		BackButton = GameObject.Find ("BackBtn");
		RetryButton = GameObject.Find ("ReplayBtn");
		NextLevelButton = GameObject.Find ("NextBtn");

		BackButton.SetActive (false);

		if(RetryButton != null)
		RetryButton.SetActive (false);

		if(NextLevelButton != null)
		NextLevelButton.SetActive (false);

		if(stars != null)
		stars.SetActive (false);

		if (GameObject.Find ("googleReviewMainButton") != null) {
			toggleReview = GameObject.Find ("googleReviewMainButton");
			toggleReview.GetComponent<tk2dUIItem>().SendMessageOnClickMethodName = "DeactivatateReviewButtonsFromDialogueBox";
		}

		//this.gameObject.SetActive (false);
	}


	public void reactiveReviewDialogue() {
		if(SendReviewButton == null)
		SendReviewButton.SetActive (true);
	}

}
