﻿using UnityEngine;
using System.Collections;

public class ParticlesOnStars : MonoBehaviour {

	// Use this for initialization
	int noOfStarsDeserved = 0;
	GameObject StarParticle1, StarParticle2, StarParticle3;

	void Awake() 
	{
		StarParticle1 = GameObject.Find ("UIParent/LevelCompletePanel/Stars/LevelCompleteStarFilled1/CFX4 Secret Place Loop 1");
		StarParticle2 = GameObject.Find ("UIParent/LevelCompletePanel/Stars/LevelCompleteStarFilled2/CFX4 Secret Place Loop 1");
		StarParticle3 = GameObject.Find ("UIParent/LevelCompletePanel/Stars/LevelCompleteStarFilled3/CFX4 Secret Place Loop 1");

	}
	public void StartOnCompletion() 
	{
		noOfStarsDeserved = PlayerPrefs.GetInt ("boltTaken");
		//Debug.Log (noOfStarsDeserved);
		if (noOfStarsDeserved == 1) {
			StarParticle1.SetActive (false);
			StarParticle2.SetActive (true);
			StarParticle2.transform.localPosition = new Vector3(-2.5f, -1 ,-5);
			StarParticle3.SetActive (false);
		}

		if (noOfStarsDeserved == 2) {
			StarParticle1.SetActive (true);
			StarParticle2.SetActive (true);
			StarParticle2.transform.localPosition = new Vector3(0, 0 ,-5);

			StarParticle3.SetActive (false);
		}

		if (noOfStarsDeserved == 3) {
			StarParticle1.SetActive (true);
			StarParticle2.SetActive (true);
			StarParticle2.transform.localPosition = new Vector3(2.5f, -1 ,-5);

			//StarParticle2.transform.localPosition = new Vector3();
			StarParticle3.SetActive (true);
		}

		if (noOfStarsDeserved == 0) {
			StarParticle1.SetActive (false);
			StarParticle2.SetActive (false);	
			StarParticle3.SetActive (false);
		}
		
	}

}
