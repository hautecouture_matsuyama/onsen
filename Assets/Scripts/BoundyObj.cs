﻿using UnityEngine;
using System.Collections;

public class BoundyObj : MonoBehaviour {

    bool wait;

    [SerializeField]
    float boundRate = 1.0f;

    void OnCollisionEnter(Collision col)
    {
        StartCoroutine(ApplyBound(col.gameObject));
    }

    IEnumerator ApplyBound(GameObject go)
    {
        if (wait) yield break;

        string name = go.name.ToLower();
        //ロボのパーツかチェック.
        if (name.Contains("robo"))
        {
            Transform robo = go.transform.parent;
            foreach (Transform part in robo)
            {
                Vector3 vec = part.GetComponent<Rigidbody>().velocity;
                vec.y *= -2.2f;
                float angle = transform.parent.eulerAngles.z;
                angle = (angle > 180) ? 360 - angle : -angle;
                vec.x += 1.5f * angle / 45;
                vec *= boundRate;

                part.GetComponent<Rigidbody>().AddForce(vec, ForceMode.VelocityChange);
            }
        }
        wait = true;
        yield return new WaitForSeconds(0.5f);
        wait = false;
    }
}
