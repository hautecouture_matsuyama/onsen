﻿using UnityEngine;
using System.Collections;

public class ClawScript : MonoBehaviour
{
		private float speed = 8f, xLimit = 0;
		private bool translateClawnForward = false;

        [SerializeField]
        private GameObject playObject;
        private Rigidbody2D playRigidbody2D;
     
		// Use this for initialization
		void Start ()
		{
			Camera.main.GetComponent<SoundManager> ().playAnvilSound ();
            playRigidbody2D = playObject.GetComponent<Rigidbody2D>();
		}

		void Update ()
		{
				if (translateClawnForward) {
						transform.Translate (speed*Time.deltaTime, 0, 0);
						if (transform.position.x >= xLimit) {
								transform.position = new Vector3 (xLimit, transform.position.y, transform.position.z);
								translateClawnForward = false;
						}
				}

                ShakeClaw();
		}
        Coroutine curCo;
    
        void ShakeClaw()
        {
            if (Input.GetAxisRaw("Horizontal") != 0 && curCo == null)
            {
                curCo = StartCoroutine(ShakeClawCo(Input.GetAxisRaw("Horizontal") * 50, 0.5f));
            }
        }

        IEnumerator ShakeClawCo(float destAngle, float time = 1.0f)
        {
            float startTime = 0;

            Quaternion from = transform.rotation;
            Quaternion to = Quaternion.EulerAngles(0, 0, destAngle);

            while (startTime < time) {
                transform.rotation = Quaternion.Slerp(from, to, (startTime / time ));
                startTime += Time.deltaTime;
                yield return new WaitForEndOfFrame ();
            }

            float nextAngle = -destAngle * 0.8f;
            if (Mathf.Abs(nextAngle) < 10f)
            {
                curCo = null;
                yield break;
            }
            StartCoroutine(ShakeClawCo(-destAngle * 0.5f, time));
        }

        //リリース
		public void setHandRotation ()
		{
				gameObject.layer = 0;
                this.gameObject.transform.DetachChildren();
                this.gameObject.SetActive(false);

                playObject.transform.SetParent(GameObject.FindGameObjectWithTag("Scene").transform);

                playObject.AddComponent<Rigidbody2D>();
                
		}
		//will set limit of clawn for moving initially
		public void setXlimit (float limit)
		{
				xLimit = limit;
				translateClawnForward = true;
                
		}
}
