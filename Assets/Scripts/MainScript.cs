using UnityEngine;
using System.Collections;

public class MainScript : MonoBehaviour
{

		GameObject filledStar1, filledStar2, filledStar3;
		public int countForNoOfStars = 0;
		StarScoreScript vaiableStarScoreScript;
		SoundManager manageSound;
		public GameObject uiManager, clawPrefeb,playerPrefab, redButton, greenButton, particleSystemObject, crushedTextObjectPrefeb, fireParticle, replayBtn;
		public tk2dTextMesh levelDigit;
		public RuntimeAnimatorController replayAnimation;
		private int currentLevel = 1;
		private UIDelegate delegate_;
		private GameObject scene, anvil, gameClaw, playerObj;
		private Vector3 initialPosition, movePosition, positionDiff;
		private bool moving = false, roboReleased = false, isLevelCompleted = false, initialDataLoaded = false, isReplayAnimationStarted = false;
		private float crusherCollisionTime = 0;
		private int boltTaken = 0;
		private float[] clawEndPoints = {
				0,          //Stage01
				-8.5f,
				8.5f,
				0,
				-8.5f,      //Stage05
				-8.5f,
				-6.6f,
				0,
				0,
				8.5f,       //Stage10
				-5,
				-8.5f,
				-3,
				-8.5f,
				-8.5f,      //Stage15
				8.5f,
				0,
				0,
				4,
				7,          //Stage20
                -8f,
                -8.5f,
                10,
                7,
                6,          //Stage25
                0,
                7,
                -3,
                8.5f,
                7,          //Stage30
		};

		void Awake() {
		if(greenButton != null)
			greenButton.SetActive(false);

		manageSound = GameObject.FindObjectOfType (typeof(SoundManager)) as SoundManager;
		
	}
		void Start ()
		{
		vaiableStarScoreScript = GameObject.FindObjectOfType<StarScoreScript>() as StarScoreScript;


	}
	
		public void loadInitialData (float maxLimit, int levelNum, GameObject sceneInstance)
		{
				boltTaken = 0;
				isLevelCompleted = false;
				crusherCollisionTime = 0;
				isReplayAnimationStarted = false;
				replayBtn.GetComponent<Animator> ().runtimeAnimatorController = null;
				delegate_ = new UIDelegate ();
				currentLevel = levelNum;
				redButton.SetActive (true);
				greenButton.SetActive(false);
				scene = sceneInstance;
				levelDigit.text = "Level : " + currentLevel;
				initializeclaw ();
				initializerobot ();
				roboReleased = false;

				initialDataLoaded = true;

		}

		private void initializeclaw ()
		{
				gameClaw = (GameObject)Instantiate (clawPrefeb, clawPrefeb.transform.position, Quaternion.identity);
				gameClaw.transform.parent = scene.transform;
				gameClaw.GetComponent<ClawScript> ().setXlimit (clawEndPoints [currentLevel - 1]);
				gameClaw.transform.parent = scene.transform;
		}

    //温泉まんじゅうくん生成
		private void initializerobot ()
		{
                //gameRobot = (GameObject)Instantiate (robotPrefeb, robotPrefeb.transform.position, Quaternion.identity);
                //foreach (Transform child in gameRobot.transform) {
                //        if (child.name.Equals ("Robo_Head")) {
                //                gameClaw.GetComponent<HingeJoint> ().connectedBody = child.rigidbody;
                //                break;
                //        }
                //}
                //gameRobot.transform.parent = scene.transform;
		}

		void Update ()
		{
				if (!initialDataLoaded)
						return;
				if (roboReleased) {
						if (!isReplayAnimationStarted && crusherCollisionTime != 0 && Time.timeSinceLevelLoad - crusherCollisionTime >= 10) {
								isReplayAnimationStarted = true;
								startReplayAnimation ();
						}
						return;
				}
				checkForTouchActions ();
		}

		private void checkForTouchActions ()
		{
				if (Input.GetMouseButtonDown (0)) {

                    Vector2 worldPoint = Camera.main.ScreenToWorldPoint( Input.mousePosition);
                   

						moving = true;
                        RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);
						

                    if(hit.collider != null && hit.collider.tag ==  "Anvil")
                    {
                        

                        anvil = hit.collider.gameObject;
                        initialPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
                        positionDiff = Camera.main.ScreenToWorldPoint (Input.mousePosition) - anvil.transform.position;
                    }
       


                        //        if (hit.collider != null) {
                        //                if (hit.collider.transform.CompareTag ("Anvil")) {
                        //                    Debug.Log("hittt");
                        //                        anvil = hit.collider.gameObject;
                        //                        initialPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
                        //                        positionDiff = Camera.main.ScreenToWorldPoint (Input.mousePosition) - anvil.transform.position;
                        //                } else if (hit.collider.transform.CompareTag ("AnvilChild")) {
                        //                        anvil = hit.collider.transform.parent.gameObject;
                        //                        initialPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
                        //                        positionDiff = Camera.main.ScreenToWorldPoint (Input.mousePosition) - anvil.transform.position;
                        //                }
								
                        //}
				}
				if (Input.GetMouseButtonUp (0)) {
						moving = false;
						anvil = null;
						RaycastHit hit;
						Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
						if (Physics.Raycast (ray, out hit)) {
                            //Goボタン押した.
								if (hit.collider != null && hit.collider.transform.CompareTag ("Red_Button")) {
										redButton.SetActive (false);
										greenButton.SetActive(true);

					                    if(manageSound.isSoundOn == false){
						                    //GameObject.Find("GreenButton").GetComponent
						                    greenButton.GetComponent<AudioSource>().Stop();
					                    }
					                    else 
						                    greenButton.GetComponent<AudioSource>().Play();
				
										releaseRobot ();
								}
						}
				}
				if (moving && anvil) {
						anvil.transform.position = Camera.main.ScreenToWorldPoint (Input.mousePosition) - positionDiff;
						anvil.transform.position = new Vector3 (anvil.transform.position.x, anvil.transform.position.y, -0.6f);
			    }
		}

		public void gameOver ()
		{
            Debug.Log("AAAAAAAAAAAAAAAAAa");

				setDefaultValues ();
				delegate_.RaiseOnLevelFail ();
		PlayerPrefs.SetInt ("boltTaken", boltTaken);



	//	GameObject.FindObjectOfType<ParticlesOnStars> ().StartOnCompletion ();
		}


        //ゲームクリア後の星
		public void levelComplete ()			// the screen will change to scoreboard
		{
            

			StopAllCoroutines ();
			setDefaultValues ();

			delegate_.RaiseOnLevelComplete ();

            filledStar1 = GameObject.Find("UIParent/LevelCompletePanel/Stars/LevelCompleteStarFilled1");		// resetting animations of stars collected
            filledStar2 = GameObject.Find("UIParent/LevelCompletePanel/Stars/LevelCompleteStarFilled2");
            filledStar3 = GameObject.Find("UIParent/LevelCompletePanel/Stars/LevelCompleteStarFilled3");

            filledStar1.GetComponent<SpriteRenderer>().enabled = false;
            filledStar2.GetComponent<SpriteRenderer>().enabled = false;
            filledStar3.GetComponent<SpriteRenderer>().enabled = false;


            switch(boltTaken)
            {
                case 1:
                    filledStar1.GetComponent<SpriteRenderer>().enabled = true;
                    break;
                case 2:
                    filledStar2.GetComponent<SpriteRenderer>().enabled = true;
                    break;
                case 3:
                    filledStar3.GetComponent<SpriteRenderer>().enabled = true;
                    break;
            }


		    filledStar1.GetComponent<TweenScale> ().ResetToBeginning ();
			filledStar2.GetComponent<TweenScale> ().ResetToBeginning ();
			filledStar3.GetComponent<TweenScale> ().ResetToBeginning ();

			filledStar1.GetComponent<TweenScale> ().PlayForward ();
			filledStar2.GetComponent<TweenScale> ().PlayForward ();
			filledStar3.GetComponent<TweenScale> ().PlayForward ();

			filledStar1.transform.localScale = new Vector3 (0, 0, 0);
			filledStar2.transform.localScale = new Vector3 (0, 0, 0);
			filledStar3.transform.localScale = new Vector3 (0, 0, 0);

			Debug.Log ("wonderfooooooooooooooooooooooool");
			PlayerPrefs.SetInt ("boltTaken", boltTaken);
			GameObject.FindObjectOfType<ParticlesOnStars> ().StartOnCompletion ();

	}

		void setDefaultValues ()
		{
				initialDataLoaded = false;
				Camera.main.transform.position = new Vector3 (0, 0, -50);
		}

    //ボタン押された後？
		private void releaseRobot ()
		{
				roboReleased = true;
				foreach (BoxCollider boxes in gameClaw.GetComponentsInChildren<BoxCollider>())
						Destroy (boxes as BoxCollider);

				HingeJoint joint = gameClaw.GetComponent<HingeJoint> ();
				Destroy (joint as HingeJoint);

                //foreach (HingeJoint hJoint in gameRobot.GetComponentsInChildren<HingeJoint>())
                //        hJoint.useLimits = false;
				
				//SetLayerRecursively (gameRobot, 0);
				SetLayerRecursively (gameClaw, 0);

				scene.BroadcastMessage ("isRoboreleased", true, SendMessageOptions.DontRequireReceiver);

				gameClaw.GetComponent<ClawScript> ().setHandRotation ();
		}

		public bool isRoboReleased ()
		{
				return roboReleased;
		}

    IEnumerator ShowInter()
        {

        yield return new WaitForSeconds(Random.Range(0.2f,0.8f));

        if (Random.value <= 0.33)
            NendUnityPlugin.AD.NendAdInterstitial.Instance.Show();

        }

		public void gameEnd ()
		{
            StartCoroutine(ShowInter());

			if (isLevelCompleted) {
						levelComplete ();
						//Debug.Log (boltTaken);
						//	GameObject.FindObjectOfType<ReviewPanelScript>().Start();
						GameObject.FindObjectOfType<ReviewPanelScript> ().DeactivatateReviewButtons ();

				} else {
						gameOver ();
			GameObject.FindObjectOfType<ReviewPanelScript> ().DeactivatateReviewButtons ();

				}
		}

		public int getBoltTaken ()
		{
				return boltTaken;
		}

		public void increaseBoltTaken ()
		{
				boltTaken++;
		}

		private void setLevelCompleteTrue ()
		{
				isLevelCompleted = true;
		}

		private void startReplayAnimation ()
		{
				replayBtn.GetComponent<Animator> ().runtimeAnimatorController = replayAnimation;
		}

		public void instantiateCollisionParticles (Vector3 position)
		{
				GameObject temp = (GameObject)Instantiate (particleSystemObject, position, particleSystemObject.transform.rotation);
				temp.transform.parent = scene.transform;

		}

		public void instantiateCrushedText ()
		{
				setLevelCompleteTrue ();
				GameObject tmp = (GameObject)Instantiate (crushedTextObjectPrefeb, crushedTextObjectPrefeb.transform.position, Quaternion.identity);
				StartCoroutine (StartGameEndAnimation (2));
				tmp.transform.parent = scene.transform;
		}

		public void BombCollided (Vector3 position)
		{
				Instantiate (fireParticle, position, Quaternion.EulerAngles (-90, 0, 0));
				StartCoroutine (StartGameEndAnimation (3));
		}

		public void setCrusherCollisionTime ()
		{
				if (crusherCollisionTime == 0)
					crusherCollisionTime = Time.timeSinceLevelLoad;
		}
		//if robot is collided with lower boundry seconds
		public IEnumerator StartGameEndAnimation (float time)
		{
				float i = 0;
				float rate = 1 / time;
				while (i < 1) {
						i += rate * Time.deltaTime;
						yield return 0;
				}

				gameEnd ();
                GameEndDetectionScript.cnt = 0;
		}

		public static void SetLayerRecursively (GameObject objectToChange, int layerNumber)
		{
			foreach (Transform trans in objectToChange.GetComponentsInChildren<Transform>(true)) {
					trans.gameObject.layer = layerNumber;
			}
		}
}