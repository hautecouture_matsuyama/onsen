﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetManju : MonoBehaviour {


    [SerializeField]
    private GameObject[] titleManju;

    private GameObject activeManju;

	void OnEnable()
    {
        int idx = 0;

        for(int i = 1; i<21; i++)
        {
            idx += PlayerPrefs.GetInt("Level"+i);
        }

        int cnt = idx / 10;


        switch(cnt)
        {
            case 0:
                activeManju = titleManju[0];
                break;
            case 1:
            case 2:
                activeManju = titleManju[1];
                break;
            case 3:
            case 4:
            case 5:
                activeManju = titleManju[2];
                break;
            case 6:
                activeManju = titleManju[3];
                break;

        }


        activeManju.SetActive(true);


    }

    void OnDisable()
    {
        activeManju.SetActive(false);
 
    }

 


}
