﻿using UnityEngine;
using System.Collections;

public class Howto : MonoBehaviour {


    [SerializeField]
    private BoxCollider red_buuton;
	


    void OnEnable()
    {
        Time.timeScale = 0;
        red_buuton.enabled = false;
    }

    void OnDisable()
    {
        Time.timeScale = 1;
        Invoke("Colli", 1);
    }

    void Colli()
    {
        red_buuton.enabled = true;
    }
}
