﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameTitle : MonoBehaviour {

    [SerializeField]
    private Text copyright;

	void OnEnable()
    {
        copyright.enabled = true;
    }

    void OnDisable()
    {
        copyright.enabled = false;
    }
}
