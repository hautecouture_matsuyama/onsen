﻿using UnityEngine;
using System.Collections;

public class SpawanExsample : MonoBehaviour {

    [SerializeField]
    private GameObject parentObj;

    [SerializeField]
    private GameObject zabuton;

    private GameObject childObj;


	// Update is called once per frame
	void Update () {

     if(Input.GetKeyUp(KeyCode.Space))
     {
         for (int i = 0; i < 30; i++)
         {
             childObj = (GameObject)Instantiate(zabuton, this.transform.position, Quaternion.identity);
             childObj.GetComponent<SpriteRenderer>().sortingOrder = i;
             childObj.transform.parent = parentObj.transform;
         }

         
     }
	
	}
}
