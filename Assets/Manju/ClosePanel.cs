﻿using UnityEngine;
using System.Collections;

public class ClosePanel : MonoBehaviour {

    [SerializeField]
    private GameObject hidePanel;

    public void Hide()
    {
        StartCoroutine(HideCoroutine());
    }

    IEnumerator HideCoroutine()
    {
        hidePanel.SetActive(false);
        yield return new WaitForSeconds(1f);
        
    }

}
